from bs4 import BeautifulSoup
import requests
from .models import TableScrap

#import pandas as pd

class DateScrap():

    url = 'https://resultados.as.com/resultados/futbol/primera/clasificacion/'
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')

    #Equipos
    eq = soup.find_all('span', class_='nombre-equipo')

    equipos = list()
    count = 0
    for i in eq:
        if count < 20:
            equipos.append(i.text)
        else:
            break
        count += 1

    print(equipos)

    #Puntuacion
    pu = soup.find_all('td', class_='destacado')

    puntuacion = list()
    count2 = 0
    for i in pu:
        if count2 < 20:
            puntuacion.append(i.text)
        else:
            break
        count2 += 1

    print(puntuacion)

    for i in range(len(puntuacion)):
        team = TableScrap()
        team.name_team = equipos[i]
        team.punctuation = puntuacion[i]
        team.save()
