from django.contrib import admin

from .models import TableScrap


class TableScrapAdmin(admin.ModelAdmin):
    list_display=['name_team', 'punctuation', 'datetime']

admin.site.register(TableScrap, TableScrapAdmin)


