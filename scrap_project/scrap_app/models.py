from django.db import models

class TableScrap(models.Model):
    name_team = models.CharField(max_length=200)
    punctuation = models.CharField(max_length=50)
    datetime = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name_team
