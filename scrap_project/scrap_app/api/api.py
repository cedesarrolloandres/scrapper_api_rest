from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from scrap_app.api.serializers import (
    TableScrapSerializer
)

from scrap_app.models import TableScrap

@api_view(['GET','POST'])
def table_api_view(request):
    if request.method == 'GET':
        table_scrap = TableScrap.objects.all()
        table_scrap_serializer = TableScrapSerializer(table_scrap, many=True)
        return Response(table_scrap_serializer.data, status=status.HTTP_200_OK)

    if request.method == 'POST':
        table_scrap_serializer = TableScrapSerializer(data=request.data)
        if table_scrap_serializer.is_valid():
            table_scrap_serializer.save()
            return Response({'Mesaage': 'Created successfully'}, status=status.HTTP_201_CREATED)
        return Response(table_scrap_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
def table_detail_view(request, pk):
    _table_scrap = TableScrap.objects.filter(id=pk)

    if _table_scrap.exists():
        _table_scrap = _table_scrap.latest('id')

        if request.method == 'GET':
            table_scrap_serializer = TableScrapSerializer(_table_scrap)
            return Response(table_scrap_serializer.data, status=status.HTTP_200_OK)

        if request.method == 'PUT':
            table_scrap_serializer = TableScrapSerializer(_table_scrap, data=request.data)
            if table_scrap_serializer.is_valid():
                table_scrap_serializer.save()
                return Response({'Message: ': 'The equipment was updated'}, status=status.HTTP_200_OK)
            return Response(table_scrap_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        if request.method == 'DELETE':
            _table_scrap.delete()
            return Response({'Message: ': 'the team was deleted'}, status=status.HTTP_200_OK)

    return Response({'Message: ': 'The team does not exist'}, status=status.HTTP_404_NOT_FOUND)