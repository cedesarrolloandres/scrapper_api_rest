from django.urls import path
from scrap_app.api.api import table_api_view, table_detail_view

urlpatterns = [
    path('table_scrap/', table_api_view, name='table_api_view'),
    path('table_scrap/<int:pk>', table_detail_view, name='table_detail_view'),
]

#http://127.0.0.1:8000/scrap/table_scrap/