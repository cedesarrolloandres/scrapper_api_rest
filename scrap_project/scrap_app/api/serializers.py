from rest_framework import serializers
from scrap_app.models import TableScrap

class TableScrapSerializer(serializers.ModelSerializer):
    class Meta:
        model = TableScrap
        fields = '__all__'
        #exclude = ('name_team')


#Esto es un ejemplo de como crear una SERIALIZACION INDEPENDIENTE
"""
class CustomPersonSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    last_name = serializers.CharField(max_length=200)
    email = serializers.EmailField()

    def create(self, validated_data):
        return Person.objects.create(**validated_data)

    def update(self, instance, validated_data):
        updated = super().update(instance, validated_data)
        return updated
"""